import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(LoginScreen());
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Login Screen")),
      body: Column(
        children: [
          const Text(
              "Please fill in your login details, if it's your first time with us, scroll down and register. "),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Username/Email"),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Password"),
            ),
          ),
          ElevatedButton(
            onPressed: () {},
            child: const Text("Login"),
          ),
          const Text(
              "Please fill in your login details, if it's your first time with us, scroll down and register. "),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Name"),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Email"),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Mobile Number"),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: const EdgeInsets.symmetric(horizontal: 40),
            child: const TextField(
              decoration: InputDecoration(labelText: "Create a password"),
            ),
          ),
          ElevatedButton(
            onPressed: () {},
            child: const Text("Register"),
          ),
        ],
      ),
    );
  }
}
